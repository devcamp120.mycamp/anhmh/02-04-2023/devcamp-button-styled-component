import styled from "styled-components";

const PropButton = styled.button`
    background-color: ${props => props.bgColor || 'red' };
    color: ${props => props.fontColor || 'white' }; 
    border: none;
    border-radius: 5px;
    font-size: 14px;
    padding: 10px 20px;
    margin: 5px
    `;

const PrimaryButton = styled.button `
    ${props => props.primary ? 
    `background-color: #7A4CD7; color: white; ` : 
    `background-color: #FF31CA; color: white; ` }
    background-color: ${props => props.primary ? '#7A4CD7' : '#FF31CA' };
    color: ${props => props.primary ? 'white' : '#fffff0' };         
    border: none;
    border-radius: 5px;
    font-size: 14px;
    padding: 10px 20px;
    margin: 5px
    `;

// const PrimaryButton = styled.button `
//     background-color: ${props => props.primary ? '#7A4CD7' : '#FF31CA' };
//     color: ${props => props.primary ? 'white' : '#fffff0' };         
//     border: none;
//     border-radius: 5px;
//     font-size: 14px;
//     padding: 10px 20px;
//     margin: 5px
//     `;

export { PropButton, PrimaryButton } ;