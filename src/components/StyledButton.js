import styled from "styled-components";

export default styled.button({
    backgroundColor: "#7A4CD7",
    color: "white",
    border: "none",
    borderRadius: "5px",
    padding: "10px",
    margin: "10px",
})

/*
styled ['button'] `
background-color: #7A4CD7;
color: white;
border: none;
border-radius: 5px;
padding: 10px;
margin: 10px;
`
*/

/*
styled.button `
    background-color: #7A4CD7;
    color: white;
    border: none;
    border-radius: 5px;
    padding: 10px;
    margin: 10px;
`
*/