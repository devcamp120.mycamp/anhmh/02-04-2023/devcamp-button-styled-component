import { ChildButton, SuperButton } from "./components/ExtendButton";
import { PropButton,  PrimaryButton } from "./components/PropButton";
import StyledButton from "./components/StyledButton";

function App() {
  return (
    <div>
      <>
        <StyledButton>I am a styled button!</StyledButton>
        <br></br>
        <>
        <PropButton bgColor="#FFC2C3" fontColor="#B72322">Button 1</PropButton>
        <PropButton bgColor="#FFD9CA" fontColor="#DA7971">Button 2</PropButton>
        <PropButton bgColor="#FFF3C7" fontColor="#BEA772">Button 3</PropButton>
        <PropButton>Button 4</PropButton>
        </>
        <br></br>
        <>
        <PrimaryButton primary>Primary Button</PrimaryButton>
        <PrimaryButton>Other Button 1</PrimaryButton>
        <PrimaryButton>Other Button 2</PrimaryButton>
        </>
        <br></br>
        <>
          <SuperButton>Super Button</SuperButton>
          <ChildButton bgColor="#FF31CA" fontColor="#ffffff">Hello!</ChildButton>
          <ChildButton bgColor="#FF68B4" fontColor="#ffffff" >Hello!</ChildButton>
          <ChildButton bgColor="#FF7F4F" fontColor="#ffffff" >Hello!</ChildButton>
        </>
      </>
    </div>
  );
}

export default App;
